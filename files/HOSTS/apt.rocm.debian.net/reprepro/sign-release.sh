#!/usr/bin/bash
# See reprepro(1), "SignWith"

FILETOSIGN="$1"
INLINESIG="$2"
DETACHSIG="$3"

if [ -n "$INLINESIG" ]
then
	/usr/bin/gpg \
		--batch \
		--with-colons \
		--pinentry=loopback \
		--passphrase-file=$HOME/.gnupg/private-keys-v1.d/passphrase \
		--clear-sign \
		--output "$INLINESIG" \
		"$FILETOSIGN"
fi

if [ -n "$DETACHSIG" ]
then
	/usr/bin/gpg \
		--batch \
		--with-colons \
		--pinentry=loopback \
		--passphrase-file=$HOME/.gnupg/private-keys-v1.d/passphrase \
		--clear-sign \
		--detach-sign \
		--output "$DETACHSIG" \
		"$FILETOSIGN"
fi
