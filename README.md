# Debian ROCm Team Infrastructure

This is the [Ansible](https://www.ansible.com)-based configuration of the
Debian ROCm Team's infrastructure.


## Overview

- [apt.rocm.debian.net](https://apt.rocm.debian.net) - our own APT archive
- [ci.rocm.debian.net](https://ci.rocm.debian.net) - debci master and frontend
- Various workers (can be public or private)

### Deployment

Using the `ssh` transport, so your key must be in `authorized_keys`. Also,
some of the files within this repository are encrypted using Ansible Vault
together with GnuPG (see [gpg-passphrase-client.sh](bin/README.md)). Contact
@ckk to get both set up.


## Adding a CI Worker

Workers can be managed either manually or using Ansible. However, even
Ansible-managed hosts need some manual preparation first.

Workers should generally target the `bookworm` distribution.

### Manual Preparation (all hosts)

1. Add our APT archive following the [Instructions for
   Users](https://apt.rocm.debian.net/index.html#instructions-for-users),
   again targeting the `bookworm` distribution.

   This is a **MUST**. Our infra needs forked versions of `debci`,
   `autopkgtest`, and possibly other packages.

1. Prepare the system for use with the `qemu` or `podman` backends, optionally
   as an unprivileged user if you prefer that.
 
   Packages `rocm-qemu-support` and `rocm-podman-support`, available in
   `unstable` release of our APT archive, automate or simplify most of this.
   Just run `rocm-qemu-setup` resp. `rocm-podman-setup`. See also
   [rocm-dev-tools](https://salsa.debian.org/rocm-team/rocm-dev-tools).

Then, proceed with either the manual or the managed configuration.

### Manual configuration

1. Install `debci-worker` from our APT archive.

1. Request a client certificate from @ckk (until [#1037322](https://bugs.debian.org/1037322)
   and [#1038139](https://bugs.debian.org/1038139) have been resolved), and
   place the certificate, the key, and the
   [CA cert](files/HOSTS/ci.rocm.debian.net/debci/ca.crt) into `/etc/debci`. The key should only be
   readable by `debci`.

1. Create a file `/etc/debci/conf.d/debian-common.conf`, with the following contents:

   ```
   # We treat the certficate as authentication for now, so ignore the 'debci:debci'
   debci_amqp_server="amqps://debci:debci@ci.rocm.debian.net"
   debci_amqp_ssl=true
   debci_amqp_cacert=/etc/debci/ca.crt
   debci_amqp_cert=/etc/debci/<your-cert>
   debci_amqp_key=/etc/debci/<your-key>
   debci_backend="qemu+rocm"
   debci_suites="stable testing unstable [suiteN]*"
   debci_mirror="http://x.x.x.x:xxxx/debian"
   ```

   Replace
   - `x.x.x.x:xxxx` with the worker's IP and port where an APT cache server
     is listening (NOT `localhost` or `127.0.0.1`, that won't work from within
     the guest VM)
   - `[suiteN]*` with any additional suites you'd like to offer to the network,
     if any

1. For each desired worker configuration:

   1. Create `/etc/debci/<config>` and `/etc/debci/<config>/conf.d`

   1. Symlink `/etc/debci/<config>/conf.d/debian-common.conf -> /etc/debci/conf.d/debian-common.conf`

   1. Create `/etc/debci/<config>/debci.conf` with the following contents:

   ```
   debci_arch="amd64+gfxNNNN"
   debci_autopkgtest_args_<backend>="<args>"
   ```

   Replace
   - `gfxNNNN` with your worker's [GPU
   architecture](https://lists.debian.org/debian-ai/2023/04/msg00003.html)
   - `<backend>` with `qemurocm` or `podmanrocm`, whichever you are using
   - `<args>` with any particular backend configuration you might want to add.
   

1. Build the test environments:

   ```shell
   $ sudo debci update-worker
   ```

1. Test the local setup:

   ```shell
   $ sudo debci localtest rocrand
   ```

   The tests should all pass, and there shouldn't be any warnings like
   `amdgpu_device_initialize: AMDGPU_INFO_ACCEL_WORKING = 0`.

1. For each worker-specific `<config>` above, enable the `debci-worker-rocm@` and `debci-publisher-rocm@` service templates so that they run the workers at boot:

   ```shell
   $ sudo systemctl enable --now debci-worker-rocm@<config>
   $ sudo systemctl enable --now debci-publisher-rocm@<config>
   ```

1. Troubleshooting: stop the `debci-worker-rocm` and `debci-publisher-rocm` services (this will stop all
   template instances), and then try them in the foreground:

   ```shell
   $ sudo -u debci debci_config_dir=/etc/debci/<config> debci worker
   ```

### Automatic configuration using Ansible

1. Add the host to the [inventory](inventory.yml). The host must be added to
   the `all` and `debci_worker` inventory groups. It should also be added to
   the `approx` group, unless it already has an APT caching setup.

1. Create the `host_vars/<NEWHOST>/public.yml` file with all the variables
   necessary for configuration of the host. You can use
   [trinity](host_vars/trinity/public.yml) as a template.

1. Create a client certificate (until [#1037322](https://bugs.debian.org/1037322)
   and [#1038139](https://bugs.debian.org/1038139) have been resolved) so that
   the worker can connect to the RabbitMQ server:

   ```shell
   $ NEWHOST=ci-worker-<NEWHOST>.rocm.debian.net
   
   $ mkdir -p files/HOSTS/$NEWHOST/debci
   $ ansible-vault decrypt pki/private/ca.key.pem
   $ cd pki
   $ ./pki client $NEWHOST
   $ cd ..

   # IMPORTANT
   $ git checkout pki/private/ca.key.pem
   $ ansible-vault encrypt pki/private/$NEWHOST.key.pem

   $ cp pki/certs/ca.crt.pem files/HOSTS/$NEWHOST/debci/ca.crt
   $ cp pki/certs/$NEWHOST.crt.pem files/HOSTS/$NEWHOST/debci/client.crt
   $ cp pki/private/$NEWHOST.key.pem files/HOSTS/$NEWHOST/debci/client.key
   ```

1. Run ansible:

   ```shell
   $ ansible-playbook site.yml -u root -l <NEWHOST>
   ```

1. Add, commit, and push your changes.


## Replicating the Server Setup

1. Install the archive key following the [Instructions for
   Users](https://apt.rocm.debian.net/index.html#instructions-for-users)

1. Use the configuration of
   [apt.rocm.debian.net](host_vars/apt.rocm.debian.net) resp.
   [ci.rocm.debian.net](host_vars/ci.rocm.debian.net) as a template. Replace
   certificates, keys, keyrings, and optionally URIs.
