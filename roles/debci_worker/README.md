# debci - Worker

This role configures a host for use as a `debci` worker; that is, a host
reading and executing
[autopkgtest](https://salsa.debian.org/ci-team/autopkgtest/-/blob/master/doc/README.package-tests.rst)
jobs queued by the debci master.

This role depends on the `global_handlers` and `common` roles.

This role contains modifications that apply only to the Debian ROCm Team's
[debci](https://salsa.debian.org/rocm-team/debci) fork, specifically with
options for the custom `qemu+rocm` and `podman+rocm` backends, and the
ability to configure workers individually.


## Topics

### debci

This installs all necessary packages, including those needed by the requested
backend, and creates the global configuration file `/etc/debci/debci.conf`, as
well as one or more individual worker files in `/etc/debci/<name>/debci.conf`.

To start the workers and their respective publishers (also on boot), for each
individual config `<name>`, run

```shell
$ sudo systemctl enable --now debci-worker-rocm@<name>
# sudo systemctl enable --now debci-publisher-rocm@<name>
```

A global configuration file is currently still needed to support the daily
image rebuilds.


## Variables

```yaml
### Values shown here are the defaults

# Connection string for the RabbitMQ server to connect to
debci_amqp_server: ""

# Whether to use SSL/TLS or not. If set to true, the files
#  - ca.crt
#  - client.crt
#  - client.key
# are copied from files/HOSTS/<inventory_hostname>/debci/ to
# /etc/debci/ on the target.
debci_amqp_ssl: false


### If the default values below are not changed, then they will be left unset
### in /etc/debci/debci.conf, which means that debci will use its own defaults

# Mirror to use for package downloads:
debci_mirror: ""

# Package architecture that workers can test.
# debci default: host architecture (dpkg --print-architecture)
debci_arch: ""

# Suite to use as defaults for debci-{setup,worker,test}.
# debci default: "unstable"
debci_suite: ""

# Backend workers use to test packages
# debci_default: "lxc"
debci_backend: ""

# List of all architectures/suites/backends this worker uses in tests. These
# lists will be used by the cron job that periodically rebuilds the test
# environments.
# debci_default: A list with the single value from the variable above.
debci_arch_list: []
debci_suite_list: []
debci_backend_list: []

# Passed on through to the autopkgtest runner (not the virtualization provider)
debci_autopkgtest_args: ""

# Passed on to the qemu+rocm backend virtualization provider
debci_autopkgtest_args_qemurocm=""

# Passed on to the podman+rocm backend virtualization provider
# (Note: placeholder; the backend currently does not accept any arguments)
debci_autopkgtest_args_podmanrocm=""

# List of extra files to copy from files/HOSTS/{{ inventory_hostname}}/debci to /etc/debci
debci_conf_extra: []

# Command to execute before running a test
# If this exits non-zero, the test is not run
debci_hook_pretest: ""

# List of individual worker configs. These configs will include all of the
# variables above, plus any added here. When both global and individual
# options are set, the individual one takes preference.
#
# Each list entry is a mapping with the following keys:
#   [required]
#   name:       config name (will create /etc/debci/<name>/debci.conf)
#   [optional]
#   debci_arch:
#   debci_autopkgtest_args:
#   debci_autopkgtest_args_qemurocm:
#   debci_autopkgtest_args_podmanrocm:
# The required keys have no default values and must be specified.
debci_conf_workers: []
```


## Examples

```yaml
###
### Global settings
###

# Passwords should be avoided until #1037322 and #1038139 get fixed
debci_amqp_server: "amqps://user:pass@10.0.0.1"

# Connect to RabbitMQ using a client certificate
debci_amqp_ssl: true

# Use a fast local mirror
debci_mirror: http://my.fast.mirror/debian

# We use the qemu+rocm backend by default
debci_backend: qemu+rocm

# This host can test the 'stable', 'testing', and 'unstable' suites using the
# 'lxc' and 'qemu' backends.
debci_suite_list: [stable, testing, unstable]

# Given the above, on an amd64 worker (where debci_arch defaults to 'amd64'),
# the cron job would periodically rebuild the following test environments:
# arch   suite     backend
# ----   -----     -------
# amd64  stable    qemu+rocm
# amd64  testing   qemu+rocm
# amd64  unstable  qemu+rocm

# Increase the global test timeout to 3h (default is 1h)
debci_autopkgtest_args="--timeout-test 10800"


###
### Individual Worker settings
###

debci_conf_workers:
  - name: gfx1030
    debci_arch: amd64+gfx1030
    debci_autopkgtest_args_qemurocm="--ram-size 32768 --gpu 0a:00.0"
  - name: gfx1034
    debci_arch: amd64+gfx1034
    debci_autopkgtest_args_qemurocm="--ram-size 32768 --gpu 0b:00.0"
```

The above will create the following files:

```shell
# /etc/debci/debci.conf
debci_amqp_server="amqps://user:pass@10.0.0.1"
debci_amqp_ssl=true
debci_amqp_cacert=/etc/debci/ca.crt
debci_amqp_cert=/etc/debci/client.crt
debci_amqp_key=/etc/debci/client.key
debci_mirror="http://my.fast.mirror/debian"
debci_backend="qemu+rocm"
debci_suite_list="stable testing unstable"
debci_autopkgtest_args="--timeout-test 10800"
```

```shell
# /etc/debci/gfx1030/debci.conf
debci_amqp_server="amqps://user:pass@10.0.0.1"
debci_amqp_ssl=true
debci_amqp_cacert=/etc/debci/ca.crt
debci_amqp_cert=/etc/debci/client.crt
debci_amqp_key=/etc/debci/client.key
debci_mirror="http://my.fast.mirror/debian"
debci_arch="amd64+gfx1030"
debci_suite_list="stable testing unstable"
debci_backend="qemu+rocm"
debci_autopkgtest_args="--timeout-test 10800"
debci_autopkgtest_args_qemurocm="--ram-size 32768 --gpu 0a:00.0"
```

```shell
# /etc/debci/gfx1034/debci.conf
debci_amqp_server="amqps://user:pass@10.0.0.1"
debci_amqp_ssl=true
debci_amqp_cacert=/etc/debci/ca.crt
debci_amqp_cert=/etc/debci/client.crt
debci_amqp_key=/etc/debci/client.key
debci_mirror="http://my.fast.mirror/debian"
debci_arch="amd64+gfx1034"
debci_suite_list="stable testing unstable"
debci_backend="qemu+rocm"
debci_autopkgtest_args="--timeout-test 10800"
debci_autopkgtest_args_qemurocm="--ram-size 32768 --gpu 0b:00.0"
```
