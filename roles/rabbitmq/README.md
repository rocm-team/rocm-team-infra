# RabbitMQ server

This role minimally configures a RabbitMQ server instance.

This role depends on the `global_handlers` and the `common` roles.


## Topics

### RabbitMQ

This creates `/etc/rabbitmq/rabbitmq.conf` and `/etc/rabbitmq/conf.d`. There
isn't much to this yet, other than managing certificates for TLS connections.

### firewalld

This opens ports 5671 (amqp) and 5672 (ampqs) in the given zone.


## Variables

```yaml
### Values shown here are the defaults

# Whether to listen on (unencrypted) TCP
rabbitmq_tcp: true

# Whether to use SSL/TLS or not. If set to true, the files
#  - ca.crt
#  - server.crt
#  - server.key
# are copied from files/HOSTS/<inventory_hostname>/rabbitmq/ to
# /etc/rabbitmq/ on the target.
rabbitmq_ssl: false

# If set, opens ports 5671 and 5672 in the named zone, rather than the default
# zone
rabbitmq_firewalld_zone: ""

# List of users to create resp. update (including permissions), where an entry
# is a mapping with the following keys:
#   [required]
#   name:
#   password:
#   [optional]
#   vhost:           default: "/"
#   configure_priv:  default: ".*"
#   read_priv:       default: ".*"
#   write_priv:      default: ".*"
# The required keys have no default, and must be specified. The last four keys
# will be used as arguments to `rabbitmqctl set_permissions`.
rabbitmq_users: []
```


## Examples

```yaml
# Disable unencrypted TCP
rabbitmq_tcp: false

# Enable TLS (requires files in files/HOSTS/<inventory_hostname>rabbitmq/):
rabbitmq_ssl: true

# Add/update the following users, set their permissions:
rabbitmq_users:
  - name: foo
    password: foopass
  - name: bar
    password: barpass
    vhost: "vhost-bar"
    write-priv: ""
```
