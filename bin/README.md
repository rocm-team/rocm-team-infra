# Utilities

- `gpg-passphrase-client.sh`: Manage secrets in Ansible Vault using `gpg`

  Accepts an argument `--vault-id=VAULT_ID`. If this argument is not used, then
  `VAULT_ID=default` is assumed.

  Expects a gpg-encrypted file `local/VAULT_ID-passphrase.gpg` to exist and
  will use the decrypted contents as a key to encrypt/decrypt secrets in the
  vault.

  `ansible-*` commands that need to decrypt files can make use of particular
  secret(s) by supplying one or more `--vault-id` arguments, eg:

  ```
  $ ansible-vault encrypt --vault-id=VAULT_ID@bin/gpg-passphrase-client.sh [...]
  $ ansible-playbook --vault-id=VAULT_ID@bin/gpg-passphrase-client.sh [...]
  ```

  To grant permissions for a particular secret to a user, decrypt and
  re-encrypt this file for all existing users + the new user (using
  `--recipient`).

  The current keys have been generated with:

  ```shell
  dd if=/dev/random bs=1 count=128 | gpg -e -a -o local/<VAULT_ID>.gpg --recipient ...
  ```
