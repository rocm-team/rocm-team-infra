# Admin Cheat Sheet

This is a collection of commonly used commands when performing administrative
tasks within our infrastructure.


## Ansible

### Inventories

We manage two sets of environments:

* Testing: [inventory-test.yml](../inventory-test.yml)

  For evaluating new hardware, and testing new software features.

* Production (default): [inventory.yml](../inventory-test.yml)

The inventory can be selected with the `-i` flag.

### Host configuration

All roles are configured through various variables set in their
[host_vars](../host_vars) directory. These variables may refer to files in
[files/HOSTS][../files/HOSTS] (when the file is too large to usefully fit in a
variable). See the `README.md` file in the respective role root directory for
configuration instructions.


### Encrypt or decrypt a file with a particular secret

All sensitive data should be encrypted with the secret for the group of people
to whom it is relevant. See [Secrets](Secrets.md).

```
$ ansible-vault encrypt --vault-id=<VAULT_ID>@bin/gpg-passphrase-client.sh file [...]

$ ansible-vault decrypt --vault-id=<VAULT_ID>@bin/gpg-passphrase-client.sh file [...]
```

### Run playbooks

```
# Assumes user is in group sudo on remote host

# First run is a dry run (-C), to examine changes that will be made
$ ansible-playbook site.yml -l <inventory_hostname> -b -K -C
$ ansible-playbook site.yml -l <inventory_hostname> -b -K 
```

`-b` (become) defaults to `sudo`, `-K` prompts for the password.

Other useful options to add:
- `-i <file.yml>`: Specify alternative inventory file.
- `--vault-id VAULT_ID@bin/gpg-passphrase-client.sh`: Specify that encrypted
   files will be decrypted using the secret identified by the vault `VAULT_ID`. 
- `--tags tag[,tag]`: Only run plays matching this specific tag.
- `--skip-tags tag[,tag]`: Skip plays matching this specific tag.


## Managing certificates

See [pki](../pki/README.md).


## RabbitMQ

### Useful commands

```
# List queue contents
$ sudo rabbitmqctl list_queues

# Purge a queue
# This should *only* be done after the relevant jobs have been removed from 
# debci's state database
$ sudo purge_queue <name>
```


## debci

debci maintains its state in the SQLite3 database
`/var/lib/debci/data/debci.sqlite3`. The important are `jobs`, `workers`,
and `packages`. `package_statuses` is relevant for the Web UI.


## reprepro

reprepro is the software that manages are APT archive. See its
[README](../roles/reprepro/README.md).

### Useful commands

```
# Process the incoming queue (packages uploaded by users) for distribution <dist>
# <dist> is typically 'sid'
$ sudo -i -u reprepro reprepro processincoming <dist>

# "Migrate" <src> from distribution <origin> to distribution <destination>
$ sudo -i -u reprepro reprepro copysrc <src> <origin> <destination>
```
