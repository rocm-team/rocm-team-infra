# Secrets

The following is a list of secrets (keys, tokens, etc.) used within our infra.


## git

Sensitive files within the git repo are encrypted using Ansible Vault together
with GnuPG. See [gpg-passphrase-client.sh](bin/README.md).

The current secrets are:

* `debian`: Access to secrets of our own infra. Readable by @ckk, @cgmb

* `debian-uo`: Access to secrets of `pinwheel` at the University of Oregon.
  Readable by @ckk, @cgmb.


## Managing certificates

See [pki](../pki/README.md).


## Salsa OAuth2 (for Self-Service)

To use the Self-Service feature, we use Salsa to authenticate the users. To
this end, an group-owned Application token (owned by the ROCm Team) was created
in Salsa in [Settings/Applications](https://salsa.debian.org/groups/rocm-team/-/settings/applications)
following these [instructions](https://docs.gitlab.com/ee/integration/oauth_provider.html).

The current tokens were created with the following parameters:

- Name: `ci.rocm.debian.net`
- Redirect URI: `https://ci.rocm.debian.net/user/auth/gitlab/callback`
- Confidential: `yes`
- Scopes: `read_user`

- Name: `ci-dev.rocm.debian.net`
- Redirect URI: `https://ci-dev.rocm.debian.net/user/auth/gitlab/callback`
- Confidential: `yes`
- Scopes: `read_user`

- Name: `ci-test.rocm.debian.net`
- Redirect URI: `https://ci-test.rocm.debian.net/user/auth/gitlab/callback`
- Confidential: `yes`
- Scopes: `read_user`
```
