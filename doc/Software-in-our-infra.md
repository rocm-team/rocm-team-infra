[[_TOC_]]

# Software in our Infrastructure

All hosts in our infrastructure run Debian 12 `bookworm`, plus a few extra
packages from our own [APT repository](https://apt.rocm.debian.net/).

Our repository can be enabled by following the
[Instructions for Users](https://apt.rocm.debian.net/index.html#instructions-for-users).
Production hosts are expected to track the `bookworm` release of our repository. Development hosts may track the `unstable` release.


# Extra Packages

These are all either

- Forked tooling from other teams with custom modifications. Some of these
  modifications are not intended for upstream. For those that are, MRs are filed
  upstream.

- Our own tooling, not yet ready for upload to the official Archive

## Forked

### autopkgtest

[autopkgtest](https://packages.debian.org/sid/autopkgtest) is a test runner for
as-installed tests that supports many backends, including containers, QEMU,
and SSH.

We have forked this and maintain the following changes in branch
[rocm-fork](https://salsa.debian.org/rocm-team/autopkgtest/-/tree/rocm-fork?ref_type=heads):

- ~**Add support for netplan.io networking**~

  ~Starting with Bionic, Ubuntu switched from ifupdown to to netplan.io.~

  **Status**: MR merged [#322](https://salsa.debian.org/ci-team/autopkgtest/-/merge_requests/322),
  will be in `autopkgtest 5.36`

- ~**Fix command prompt waiting**~

  ~A bug in output parsing could lead to a hang, eventually resulting in termination by timeout.~

  **Status**: MR merged [#354](https://salsa.debian.org/ci-team/autopkgtest/-/merge_requests/354),
  will be in `autopkgtest 5.36`

- **Introduce timeout for lack of forward progress**

  Aborts long-running tests when nothing has been printed to stdout or stderr within
  the given time.

  **Status**: Draft MR submitted[#456](https://salsa.debian.org/ci-team/autopkgtest/-/merge_requests/456), definitely needs some real-world testing first.


### debci

[debci](https://salsa.debian.org/ci-team/debci) is a Continuous Integration
framework for Debian and derivatives. It schedules tests for workers, collects
the results, and presents them through a Web UI. Worker hosts monitor a queue
for requested jobs, and run the tests using `autopkgtest` in the locally
configured backend.

We have forked this and maintain the following changes in branch
[rocm-fork](https://salsa.debian.org/rocm-team/debci/-/tree/rocm-fork?ref_type=heads):

- **Basic arch+variant support**

  We extend the `Architecture` field, which denotes a CPU ISA, with support for
  `+modifiers`. For example, the `amd64+gfx1030` specification string
  represents an `Architecture:amd64` base system with an attached GPU of the
  `gfx1030` architecture. An `amd64+gfx1030` worker will therefore only run
  tests scheduled for that GPU architecture.

  **Status**: exploratory, but running stable over the past year. Merging back
  will require Debian Policy adaptations, and thus discussion with the larger
  community.

- **New backend `qemu+rocm`**

  This is fork of the existing `qemu` backend that customizes image building
  and runs tests using the `autopkgtest-virt-qemu+rocm` virtualization provider
  from package [rocm-qemu-support](#rocm-qemu-spport) below. To configure this
  backend, use the variable `debci_backend_args_qemurocm` in `debci.conf`. You
  can use any options understood by `autopkgtest-virt-qemu+rocm` and
  `autopkgtest-virt-qemu`. When using the `--qemu-options` option, use an equals sign and single quotes to package nested string. For example:

  ```
  debci_backend_args_qemurocm="--cpus 4 --qemu-options='cpu host'"
  ```

  Image build is customized to add the `firmware-amd-graphics` package, and
  to add the `debci` user (in the guest) to the `audio`, `render`, and
  `video` groups so that autopkgtests can make use of `/dev/kfd`. Furthermore,
  `kernel.dmesg_restrict=0` is set so that regular users can use `dmesg`, and
  there is a bind-mount from `/sys/kernel/debug/dri` to `/dri` so that regular
  users can access `amdgpu_firmware_info`.

  **Status**: exploratory, but running stable over the past year.

- **New backend `podman+rocm`**

  This builds a container image from scratch, using `debootstrap` (rather
  than using `autopkgtest-build-podman` to pull an image from Docker Hub).
  It runs tests using the `autopkgtest-virt-podman+rocm` virtualization
  provider from package [rocm-podman-support](#rocm-podman-spport) below.
  To configure this backend, use the variable
  `debci_backend_args_podmanrocm` in `debci.conf`. You
  can use any options understood by `autopkgtest-virt-podman`.

  Image build is customized to add (in the image) the `debci` user and
  `root` to the `audio`, `render`, and `video` groups so that
  autopkgtests can make use of `/dev/kfd`. Adding `root` is needed
  because of `su` use during tests, and `root` isn't really `root`.

  **Status**: Very rough draft. The biggest problem is building images
  for `debci` as `root` because namespaces and cgroups don't work well
  with `su` or `sudo`.

- **Add config-aware systemd service files**

  Upstream's service files assume that azll workers share the same
  configruation. ROCm workers frequently have different configurations,
  for example each worker may have a different GPU arch assigned.

  Follow the instructions for
  [manual configuration](https://salsa.debian.org/rocm-team/rocm-team-infra#manual-configuration)
  of a worker for setting this up.

  **Status**: solid, but nothing to merge back.

- **Add support for multiple configs to `debci update-worker`**

  By default, `debci update-worker` will update images as per the
  configuration found in `/etc/debci`.
  
  However, if in that configuration, `debci_update_worker_configs` is set, then
  its value is taken as a space-seperated lists of configuration directories
  for which to run `update-worker`.

  ```
  # in /etc/debci
  debci_update_worker="/etc/debci/debian /etc/debci/ubuntu"
  ```

  **Status**: solid, but nothing to merge back.

- **Add ability to force backend for an architecture**

  Upstream `debci` supports multiple backends, but only on a per-
  package basis. We extend this to per-architecture.

  **Status**: exploratory, but running stable over the past year.

- **Add pre- and posttest hooks**

  New config options `debci_pretest_hook` and `debci_posttest_hook`.
  Contents are executed with `sh -c`.
  
  If a pretest hook exits with  non-zero, the test is aborted (as
  in: not run). The posttest hook has no effect on the overall test,
  as outputs have been generated and need to be processed.

  **Status**: exploratory and highly experimental.

- **Add a gpuenv-aware pre-test hook utility**

  The script `/usr/share/debci/util/pre-test` can be used as a hook
  described above:

  ```shell
  debci_pretest_hook="/usr/share/debci/util/pre-test $debci_config_dir"
  ```

  This hook will query [gpuenv-server](https://salsa.debian.org/rocm-team/gpuenv-utils)
  to ensure that a GPU is ready (= not busy or unresponsive) for a test,
  and then lock it. The lock is implicitly released when the test concludes.

  **Status**: new-ish, but should be relatively solid.

- **Add a new gpuenv-aware AMQP consumer**

  This new consumer is a near 1:1 replacement for the standard `amqp-consume`.
  Its main contribution is `gpuenv`-awareness: wheras `amqp-consume` immediately
  and unconditionally executes jobs as soon as they arrive, this new
  `/usr/share/debci/util/gpuenv-ampq-consume` waits until all the GPUs it needs
  are ready for use.

  This allows for multiple worker instances to cooperatively use the same GPU(s),
  by serializing their access.

  This functionality can be enabled by setting the new `debci_amqp_consume` variable:

  ```shell
  debci_amqp_consume="/usr/share/debci/util/gpuenv-amqp-consume"
  ```

  **This conflicts with the pre-test hook script mentioned in the previous bullet.**

  **Status**: exploratory and highly experimental.

- **Add a utility for querying the GPUs that a debci config will use**

  The utility `/usr/share/debci/util/list-worker-gpus` can be used to
  query the GPUs a worker will use, given the config in `/etc/debci`
  (default) resp. the config referenced in the `debci_config_dir`
  environment variable if it is set.

  **Status**: solid.

- **Fix for nested strings in config variables**

  See [debci #6](https://salsa.debian.org/rocm-team/debci/-/issues/6).

  Status: MR filed [#280](https://salsa.debian.org/ci-team/debci/-/merge_requests/280)

- ~~**Allow symlinks in `conf.d`**~~

  **Status**: MR merged [#274](https://salsa.debian.org/ci-team/debci/-/merge_requests/274), will be in `debci 3.11`

- ~~**Add support for `--extra-apt-sources` in `debci enqueue`**~~

  **Status**: MR merged [#275](https://salsa.debian.org/ci-team/debci/-/merge_requests/275), will be in `debci 3.11`

- ~~**Get uid/gid from user 'debci' rather than filesystem**~~

  **Status**: MR obsoleted [#276](https://salsa.debian.org/ci-team/debci/-/merge_requests/276), surrounding code will be dropped in `debci 3.11`



### vmdb2

[vmdb2](https://packages.debian.org/sid/vmdb2) is a tool for creating Debian and
Ubuntu images for use with QEMU.

We have forked this and maintain the following changes in branch
[rocm-fork](https://salsa.debian.org/rocm-team/vmdb2/-/tree/rocm-fork?ref_type=heads):

- ~~**Fix Ubuntu EFI image build**~~

  ~~There's a logic bug in the grub option processing.~~

  **Status**: MR merged [#143](https://gitlab.com/larswirzenius/vmdb2/-/merge_requests/143), will be in the next `vmdb2`


## Custom

### pkg-rocm-tools

Utilities for package maintainers to simplify many tasks related to
building and testing against ROCm packages. Also provides release-wide
configuration options, such as supported GPUs, etc.

See the GitLab repo for
[src:pkg-rocm-tools](https://salsa.debian.org/rocm-team/pkg-rocm-tools)
for a full description.


### rocm-qemu-support

This package provides utilities that facilitate using ROCm within QEMU VMs
with GPU pass-through. Notably, it provides the `autopkgtest-virt-qemu+rocm`
virtualization provider which can be used to run autopkgtests with
pass-through.  Our fork of `debci` (above) makes use of this backend.

See the GitLab repo for
[src:pkg-rocm-tools](https://salsa.debian.org/rocm-team/pkg-rocm-tools)
for a full description.


### rocm-podman-support

This package provides utilities that facilitate using ROCm within podman
containers. Notably, it provides the `autopkgtest-virt-podman+rocm`
virtualization provider which can be used to run autopkgtests. Our fork
of `debci` (above) makes use of this backend.

See the GitLab repo for
[src:pkg-rocm-tools](https://salsa.debian.org/rocm-team/pkg-rocm-tools)
for a full description.



### debci-scheduler

`debci-scheduler` is a utility that monitors an APT repository for changes to
certain packages and enqueues `debci` test jobs when changes are detected.

See the GitLab repo for
[src:pkg-rocm-tools](https://salsa.debian.org/rocm-team/pkg-rocm-tools)
for a full description.

The current configs for `ci` and `ci-test` can be found here:

* `ci.rocm.debian.net`: [scheduler.conf](files/HOSTS/ci.rocm.debian.net/debci/scheduler.conf)
* `ci-test.rocm.debian.net`: [scheduler.conf](files/HOSTS/ci-test.rocm.debian.net/debci/scheduler.conf)


### gpuenv-utils

`gpuenv-utils` are a set of utilities for monitoring and facilitate
cooperative access to GPUs. The immediate use case in our infrastructure
is to enable Debian and Ubuntu workers to share access to a particular GPU.

See the GitLab report for
[src:gpuenv-utils](https://salsa.debian.org/rocm-team/gpuenv-utils)
for a full description.

