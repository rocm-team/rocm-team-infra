# PKI helper

`pki` is a very simple script for creating a root CA certificate, server
certificates, and client certificates.

It does not support configuration options or any other form of advanced
management. For that, look at `easy-rsa` or similar.


## Usage

### Prerequistes: protecting key material

*These commands must be run from within the repository root directory."

Key material is protected using Ansible Vault (see [Secrets](../doc/Secrets.md).

To use the helper, one must first unlock any necessary private keys. Usually,
this includes at least the CA key. For example:

```shell
$ ansible-vault decrypt --vault-id=debian@bin/gpg-passphrase-client.sh pki/private/ca.key.pem
```

When done with the helper, is is **critical** that the CA private key, and any
key material changed or new key material generated, are protected by Ansible
Vault before they are added to git:

```
# Reset the CA key back to its encrypted version
$ git checkout pki/private/ca.key.pem

# For keys that haven't changed, reset them as well
$ git checkout pki/private/<unchanged>.key.pem

# Encrypt new keys:
$ ansible-vault encrypt --vault-id=debian@bin/gpg-passphrase-client.sh pki/private/<new-key> [...]
```

### Common usage scenarios

*These commands must be run from within the `pki/` subdirectory.*

See also: `pki -h`.

#### Renewing certificates

1. Unlock the CA key, plus all private keys of certificates to renew, following
   the instructions from the **Prerequisites** section

2. For each host, run `pki renew commonName exipryDate`, were `commonName` is the
   FQDN of the host, and `expiryDate` is an ISO date. Example:

   ```shell
   $ ./pki renew ci.rocm.debian.net 20251231
   ```

3. (re-)Lock the keys, again as per the **Prerequisites** section

This re-uses the existing private key. Alternatively, one could also `pki revoke`
the existing certificate (which also removes the key), and create new
public/private key pairs with the `client` resp. `server` subcommands of `pki`.

### Other usage scenarios

#### Out-of-repo CSRs

It's possible to keep host-specific keys out of this repository and only sign
certificate signing requests (CSRs):

1. Grab the `openssl.cnf` file from our repo:

   ```shell
   $ curl -O https://salsa.debian.org/rocm-team/rocm-team-infra/-/raw/master/pki/openssl.cnf
   ```

2. Generate a private key and a CSR:

   ```shell
   $ fqdn=host.example.com
   $ openssl genrsa -out $fqdn.key.pem 4096; \
   $ openssl req -config openssl.cnf -new -outform PEM -nodes \
        -key $fqdn.key.pem -out $fqdn.csr -subj "/CN=$fqdn"
   ```

3. Have an admin sign the CSR, and return the certificate:

   ```shell
   $ pki csrSign fileName expiryDate
   ```
